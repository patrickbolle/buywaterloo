Accounts.onCreateUser(function(options, user) {
   // Use provided profile in options, or create an empty object
   user.profile = options.profile || {};
   if (user.profile.type = 'agent') {
     user.roles = ["agent"];
   }
   else if (user.profile.type = 'user') {
     user.roles = ["user"];
   }
   //Basic Role Set Up
   // Returns the user object
   return user;
});
