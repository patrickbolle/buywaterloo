//Publish all listings
Meteor.publish("Listings", function() {
  return Listings.find();
});

//Publish all users
Meteor.publish('theUsers', function(){
    return Meteor.users.find()
});

//Publish all news
Meteor.publish('News', function(){
    return News.find()
});

//Allow deny rules
Listings.allow({
  insert: function () { return true; },
  update: function () { return true; },
  remove: function () { return true; }
});

News.allow({
  insert: function () { return true; },
  update: function () { return true; },
  remove: function () { return true; }
});

Meteor.users.allow({
  insert: function () { return true; },
  update: function () { return true; },
  remove: function () { return true; }
});
