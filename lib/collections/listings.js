Listings = new Mongo.Collection("listings");

var Schemas = {};

Schemas.Listings = new SimpleSchema({
    featured: {
        type: Boolean,
        label: "Featured Listing?"
    },
    price: {
        type: String,
        label: "Price of Listing"
    },
    address: {
        type: String,
        label: "Address",
        max: 200
    },
    city: {
        type: String,
        label: "City",
        max: 100
    },
    province: {
        type: String,
        label: "Province"
    },
    postal: {
        type: String,
        label: "Postal Code"
    },
    category: {
        type: String,
        label: "Type of Property (House, Apartment, Condo, Other)",
        allowedValues: ['House', 'Apartment', 'Condo', 'Other']
    },
    bedrooms: {
        type: String,
        label: "# Of Bedrooms",
        allowedValues: ['1', '2', '3', '4+']
    },
    bathrooms: {
        type: String,
        label: "# Of Bathrooms",
        allowedValues: ['1', '2', '3', '4+']
    },
    squareFoot: {
        type: String,
        label: "Square Feet"
    },
    createdAt: {
      type: Date,
        autoValue: function() {
          if (this.isInsert) {
            return new Date();
          }
        }
    },
    createdBy: {
      type: String,
      autoValue: function() {
      return Meteor.user().username
      }
    },
    pictures: {
    type: [String],
    label: 'Upload Pictures (Max 20)',
    minCount: 1,
    maxCount: 20
    },
    "pictures.$": {
      autoform: {
        afFieldInput: {
          type: 'cloudinary'
        }
      }
    }
});

Listings.attachSchema(Schemas.Listings);
