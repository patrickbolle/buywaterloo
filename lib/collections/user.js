var Schemas = {};
Schemas.UserProfile = new SimpleSchema({
    picture: {
      type: String,
      optional: true,
      label: 'Profile Picture',
      autoform: {
        afFieldInput: {
          type: 'cloudinary'
        }
      }
    },
    type: {
      type: String
    },
    firstName: {
      type: String,
      optional: true
    },
    lastName: {
      type: String,
      optional: true
    },
    birthday: {
      type: Date,
      optional: true
    },
    bio: {
      type: String,
      optional: true,
      autoform: {
        rows: 4
      }
    },
    website: {
      type: String,
      optional: true
    },
    company: {
      type: String,
      optional: true
    }
  });

  Schemas.User = new SimpleSchema({
    username: {
      type: String
    },
    emails: {
         type: [Object],
         optional: true
     },
     "emails.$.address": {
       type: String,
       regEx: SimpleSchema.RegEx.Email
     },
     "emails.$.verified": {
       type: Boolean
     },
    createdAt: {
      type: Date
    },
    profile: {
      type: Schemas.UserProfile,
      optional: true
    },
    services: {
      type: Object,
      optional: true,
      blackbox: true
    },
    roles: {
      type: [String],
      blackbox: true
    }
  });

  Meteor.users.attachSchema(Schemas.User);
