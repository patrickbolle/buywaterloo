News = new Mongo.Collection("news");

var Schemas = {};

Schemas.News = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        max: 200
    },
    category: {
        type: String,
        label: "Category",
        max: 100
    },
    description: {
        type: String,
        label: "Description",
        autoform: {
          rows: 10
        }
    },
    createdAt: {
      type: Date,
        autoValue: function() {
          if (this.isInsert) {
            return new Date();
          }
        }
    },
    createdBy: {
      type: String,
      autoValue: function() {
      return Meteor.user().username
      }
    },
    image: {
    type: String,
      autoform: {
        afFieldInput: {
          type: 'cloudinary'
        }
      }
    }
});

News.attachSchema(Schemas.News);
