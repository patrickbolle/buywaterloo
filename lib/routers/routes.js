//Route for homepage
Router.route('/', function () {
  this.render('homepage'
  );
});

//Route for view listing page
Router.route('/viewlistings', function () {
  this.render('viewlistings'
  );
});

//Route for add listing page
Router.route('/addlisting', function () {
  this.render('addlisting'
  );
});

//Route for user login page
Router.route('/login', function () {
  this.render('userLogin'
  );
});

//Route for user register page
Router.route('/register', function () {
  this.render('userRegister'
  );
});

//Route for agent login page
Router.route('/agentLogin', function () {
  this.render('agentLogin'
  );
});

//Route for agent register page
Router.route('/agentRegister', function () {
  this.render('agentRegister'
  );
});

//Route for listingInfo page
Router.route('/listing/:_id', {
	name: 'listingInfo',
	template: 'listingInfo',
	data: function(){
		var currentHouse = this.params._id;
		return Listings.findOne({_id: currentHouse});
	}
});

//Route for profile page
Router.route('/profile',{
	name: 'profile',
	template: 'profile',
	data: function(){
		var currentUser = Meteor.users.findOne(Meteor.userId());
		console.log(currentUser);

		return Meteor.users.findOne(Meteor.userId());
	}
});

Router.route('/agents',function(){
	this.render('agents');
});

Router.route('/agentRegisterTest',function(){
	this.render('agentRegisterTest');
});

Router.route('/userProfile/:username', {
	name: 'userProfile',
	template: 'userProfile',
	data: function(){
		var listName = this.params.username
		var findUser = Meteor.users.find({username: listName}).fetch();
		console.log(findUser);
		return Meteor.users.findOne({ username: listName });
	}
});

//Routing for news pages
Router.route('/insertNews',function(){
	this.render('insertNews');
});
