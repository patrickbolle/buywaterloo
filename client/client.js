//This file only runs on the client
//Subscribe to the listings
Meteor.subscribe("Listings");

//Subscribe to news
Meteor.subscribe("News");

//Subscribe to agnet list
Meteor.subscribe('theUsers');

//Returns ALL listings to viewlistings page
Template.viewlistings.helpers({
    listings: function () {
      return Listings.find({});
    }
  });

//Returns RECENT listings to recentListings template + homepage
Template.recentListings.helpers({
    listings: function () {
      return Listings.find({}, {sort: {createdAt: -1}, limit: 8});
    }
  });

//Returns RECENT news posts to recentNews template
Template.recentNews.helpers({
    news: function () {
      return News.find({}, {sort: {createdAt: -1}, limit: 3});
    }
  });

//Returns select list types to type template
Template.listingType.helpers({
  type:
  [
	{typeName: "For Rent"},
	{typeName: "For Sale"}
  ]
});

//Returns select list types to category template
Template.listingCategory.helpers({
  type:
  [
	{typeName: "House"},
	{typeName: "Apartment"},
  {typeName: "Condo"},
  {typeName: "All"}
  ]
});

//Returns select list types to bedroom template
Template.listingBedroom.helpers({
  type:
  [
  {typeName: "Bedrooms"},
	{typeName: "1+"},
	{typeName: "2+"},
  {typeName: "3+"},
  {typeName: "4+"}
  ]
});

//Returns select list types to bedroom template
Template.listingBathroom.helpers({
  type:
  [
  {typeName: "Bathrooms"},
	{typeName: "1+"},
	{typeName: "2+"},
  {typeName: "3+"},
  {typeName: "4+"}
  ]
});

/* Logs user out of account */
Template.navbar.events({
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
        Router.go('/');
    },
	'click .profile':function(){
		var user = Meteor.users.findOne(Meteor.userId())
		var userID = user.id;
		Router.go('profile');
	}
});

Template.homepage.events({
	'click .btn-primary': function()
	{
		var houseId = this._id;
		console.log(houseId);
	}
});


Template.agents.events({
	'click .submit-btn': function()
	{
		var agentName = $('[name=search]').val();
		var noItem = "";
		var findUser = Meteor.users.find({username: agentName}).fetch();
		console.log(findUser);
		if(findUser != "")
		{
			Router.go('userProfile', {username:agentName});
		}
		else
		{
			noItem = "No agents found.";
			document.getElementById("noSearch").innerHTML = noItem;
		}
	}
});


Template.agents.helpers({
    'users': function(){
		return  Meteor.users.find({"profile.type": 'agent'}, {sort: {username: 1}});
    }
});

//Slick carousel config
Template.listingInfo.rendered = function() {
    $('#carousel').slick({
      dots: true,
      arrows: true
    });
  }
