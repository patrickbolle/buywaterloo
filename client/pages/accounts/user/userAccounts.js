Template.userLogin.events({
    'submit form': function(event){
        event.preventDefault();
        var username = $('[name=username]').val();
        var password = $('[name=password]').val();
        Meteor.loginWithPassword(
          username,
          password,
          function(error){
             if(error){
                 console.log(error.reason); // Output error if registration fails
                 Alerts.add(error.reason);
             } else {
                 Router.go("/"); // Redirect user if registration succeeds
             }
        });
    }
});

Template.userRegister.events({
    'submit form': function(event){
        event.preventDefault();
        var username = $('[name=username]').val();
        var email = $('[name=email]').val();
        var password = $('[name=password]').val();
        Accounts.createUser({
         email: email,
         username: username,
         password: password,
         profile: {
               type: 'user'
         }
       }, function(error){
          if(error){
              console.log(error.reason); // Output error if registration fails
              Alerts.add(error.reason);
          } else {
              Router.go("/profile"); // Redirect user if registration succeeds
          }
       });
    }
});
